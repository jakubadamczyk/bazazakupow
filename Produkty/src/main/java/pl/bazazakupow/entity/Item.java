package pl.bazazakupow.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

@Entity
public class Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @Column(length = 5000)
    private String description;
    private double price;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date creationDate = new Date();
    private String buyPlace;

    public Item() {
    }

    public Item(String name, String description, double price, String buyPlace) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.buyPlace = buyPlace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getBuyPlace() {
        return buyPlace;
    }

    public void setBuyPlace(String buyPlace) {
        this.buyPlace = buyPlace;
    }

    //wszystkie dane encji zwracane w postaci tablicy stringow
    public String[] toStringArr() {
        return new String[]{name, Double.toString(price), creationDate.toString(), buyPlace};
    }
}
