package pl.bazazakupow.operation;

import java.util.List;
import pl.bazazakupow.util.DBConnector;

public class OperationExecutor {

    public static void executeOperation(DBConnector connector, Object data, OperationType operationType, List entityList, int choosenElem) {
        //otwieramy polaczenie z baza danych
        connector.openTransaction();
        //wykonujemy odpowiednia operacje na podstawie typu
        OperationFactory.createOperation(operationType).execute(connector, data, entityList, choosenElem);
        connector.commitTransaction();
    }

}
