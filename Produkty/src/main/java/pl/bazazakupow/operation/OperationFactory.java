package pl.bazazakupow.operation;

import pl.bazazakupow.operation.strategy.CRUDStrategy;
import pl.bazazakupow.operation.strategy.CreateStrategy;
import pl.bazazakupow.operation.strategy.DeleteStrategy;
import pl.bazazakupow.operation.strategy.UpdateStrategy;

public class OperationFactory {

    public static CRUDStrategy createOperation(OperationType operationType) {
        switch (operationType) {
            case CREATE:
                return new CreateStrategy();
            case UPDATE:
                return new UpdateStrategy();
            case DELETE:
                return new DeleteStrategy();
        }
        return null;
    }
}
