package pl.bazazakupow.operation;

public enum OperationType {

    CREATE, READ, UPDATE, DELETE
}
