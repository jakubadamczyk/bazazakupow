package pl.bazazakupow.operation.strategy;

import java.util.List;
import pl.bazazakupow.util.DBConnector;

public interface CRUDStrategy {

    public void execute(DBConnector connector, Object data, List entityList, int choosenElem);

}
