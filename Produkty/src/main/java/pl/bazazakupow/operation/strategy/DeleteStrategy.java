package pl.bazazakupow.operation.strategy;

import java.util.List;
import pl.bazazakupow.util.DBConnector;

public class DeleteStrategy implements CRUDStrategy {

    @Override
    public void execute(DBConnector connector, Object data, List entityList, int choosenElem) {
        connector.getEntityManager().remove(connector.getEntityManager().merge(data));
        entityList.remove(choosenElem);
    }

}
