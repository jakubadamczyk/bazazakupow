package pl.bazazakupow.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DBConnector {

    private EntityManager entityManager;
    private EntityManagerFactory emf;

    public DBConnector() {
        emf = Persistence.createEntityManagerFactory("Zakupy");
    }

    public EntityManager getEntityManager() {
        if (entityManager == null || !entityManager.isOpen()) {
            entityManager = emf.createEntityManager();
        }
        return entityManager;
    }

    public void openTransaction() {
        getEntityManager().getTransaction().begin();
    }

    public void commitTransaction() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
