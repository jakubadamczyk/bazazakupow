/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bazazakupow.frames;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.bazazakupow.entity.Item;
import pl.bazazakupow.util.DataType;

public class MainFrameTest {

    List<Item> itemList;

    @Before
    public void setUp() {
        itemList = new ArrayList<Item>();
        Item item1 = new Item("test1", "test2", 1, "test3");
        Item item2 = new Item("test11", "test21", 2, "test31");
        Item item3 = new Item("test12", "test22", 3, "test32");
        Item item4 = new Item("test13", "test23", 4, "test33");
        itemList.add(item1);
        itemList.add(item2);
        itemList.add(item3);
        itemList.add(item4);
        MainFrame.itemList = itemList;
    }

    @Test
    public void testListToArr() {
        String[][] arr = MainFrame.ListToArr(DataType.ITEM);
        Assert.assertEquals(4, arr.length);
        Assert.assertEquals("test1", arr[0][0]);
        Assert.assertEquals("test11", arr[1][0]);
    }

    @Test
    public void testListToArrNullValue() {
        String[][] arr = MainFrame.ListToArr(null);
        Assert.assertNull(arr);
    }

    @Test(expected = java.lang.NullPointerException.class)
    public void TestListToArrFail() {
        MainFrame.itemList = null;
        MainFrame.ListToArr(DataType.ITEM);
    }

}
