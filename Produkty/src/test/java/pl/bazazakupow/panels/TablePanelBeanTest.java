package pl.bazazakupow.panels;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.bazazakupow.util.DataType;

public class TablePanelBeanTest {

    TablePanelBean tablePanelBean;

    @Before
    public void setUp() {
        tablePanelBean = new TablePanelBean();
        tablePanelBean.setPanelData(new String[]{"tytul1", "tytul2"}, DataType.ITEM);
        tablePanelBean.addRow(new String[]{"test1", "test2"});
    }

    @Test
    public void testAddRow() {
        Assert.assertEquals(1, tablePanelBean.tb.getRowCount());
    }

    @Test
    public void TestReloadTableNullData() {
        tablePanelBean.reloadTable(null);
        Assert.assertEquals(1, tablePanelBean.tb.getRowCount());
    }

    @Test
    public void TestReloadTable() {
        tablePanelBean.reloadTable(new String[][]{{"test1", "test2"}, {"test3", "test4"}});
        Assert.assertEquals(2, tablePanelBean.tb.getRowCount());
    }

    @Test
    public void TestReloadTableEmptyData() {
        tablePanelBean.reloadTable(new String[][]{});
        Assert.assertEquals(0, tablePanelBean.tb.getRowCount());
    }
}
